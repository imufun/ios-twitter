//
//  Tweet.swift
//  TwitterLBTA
//
//  Created by imran on 6/16/17.
//  Copyright © 2017 Lets Build That App. All rights reserved.
//

import Foundation
import SwiftyJSON
struct Tweet {
       
    let user : User
    let message: String
//    let username: String
    //let bio: String
    //let profileImageUrl: String
    
    
    
    init(json: JSON) {
        let userJson = json["user"]

        self.user = User(json: userJson)
        self.message = json["message"].stringValue
    }
}
