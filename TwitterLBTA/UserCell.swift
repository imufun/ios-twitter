//
//  UserCell.swift
//  TwitterLBTA
//
//  Created by imran on 6/14/17.
//  Copyright © 2017 Lets Build That App. All rights reserved.
//

import LBTAComponents

class UserCell: DatasourceCell {
    
    override var datasourceItem: Any? {
        didSet {
            //  nameLabel.text = datasourceItem as? String
            
            guard let user = datasourceItem  as? User else {return}
            nameLabel.text = user.uname
            usernameLabel.text = user.username
            biotextView.text = user.biotext             
            profileImageView.loadImage(urlString: user.profileImageUrl)
        }
    }
    
    let profileImageView: CachedImageView = {
        let imageView = CachedImageView()
        imageView.image = #imageLiteral(resourceName: "profile_image")
        imageView.layer.cornerRadius = 5
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "Imran"
        label.font = UIFont.boldSystemFont(ofSize: 16)
        return label
    }()
    
    
    let usernameLabel: UILabel = {
        let label = UILabel()
        label.text = "@Imran"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor(r:130, g:130, b:130)
        return label
    }()
    
    
    
    let biotextView: UITextView = {
        let textview = UITextView()
        textview.text = " Dec 8, 2016 - A hands-on introduction to iOS app development using Swift. ... Start Developing iOS Apps (Swift) is the perfect starting point for learning to"
        textview.font = UIFont.boldSystemFont(ofSize: 15)
        textview.backgroundColor = .clear 
        return textview
        
    }()
    
    
    let followbutton: UIButton = {
        
        
        let button = UIButton()
        button.layer.cornerRadius = 5
        button.layer.borderColor = twitterColor.cgColor
        button.layer.borderWidth = 1
        button.setTitle("FOLLOW", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitleColor(twitterColor, for: .normal)
        button.setImage(#imageLiteral(resourceName: "follow"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -8, bottom: 0, right: 0)
        return button
    }()
    
    
    override func setupViews() {
        super.setupViews()
        
        
        backgroundColor = .white
        addSubview(profileImageView)
        addSubview(nameLabel)
        addSubview(usernameLabel)
        addSubview(biotextView)
        addSubview(followbutton)
        
        separatorLineView.isHidden = false
        separatorLineView.backgroundColor = UIColor(r:230, g:230 , b:230 )
        
        
        profileImageView.anchor(self.topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 12, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        
        nameLabel.anchor(profileImageView.topAnchor, left: profileImageView.rightAnchor, bottom: nil, right: followbutton.leftAnchor, topConstant: 0, leftConstant: 12, bottomConstant: 0, rightConstant: 12, widthConstant: 0, heightConstant: 20)
        
        
        usernameLabel.anchor(nameLabel.bottomAnchor, left: nameLabel.leftAnchor, bottom: nil, right: nameLabel.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 20)
        
        biotextView.anchor(usernameLabel.bottomAnchor, left: usernameLabel.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: -4, leftConstant: -4, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 0)
        
        
        followbutton.anchor(topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 12, widthConstant: 120, heightConstant: 30)
        
        
    }
    
}
