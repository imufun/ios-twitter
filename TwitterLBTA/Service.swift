//
//  Service.swift
//  TwitterLBTA
//
//  Created by imran on 7/11/17.
//  Copyright © 2017 Lets Build That App. All rights reserved.
//

import Foundation
import TRON
import SwiftyJSON

struct Service {
    
    
    let tron = TRON(baseURL: "https://api.letsbuildthatapp.com")
    static let sharedInstance = Service()
  
    
    
    
    func fetchHomeFeed(completion: @escaping(HomeDatasource?, Error?)->()){
        
        let request: APIRequest<HomeDatasource, JSONError> = tron.request("/twitter/home")
        
        request.perform(withSuccess: {(homeDatasouce) in
            
            completion(homeDatasouce, nil)
            
        }){(err) in
            completion(nil, err)
        }
    }
    
    
    
    class JSONError: JSONDecodable {
        
        required init(json: JSON) throws {
            print("JSON ERROR")
        }
    }
}
