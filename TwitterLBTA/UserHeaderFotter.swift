//
//  Cells.swift
//  TwitterLBTA
//
//  Created by Brian Voong on 12/31/16.
//  Copyright © 2016 Lets Build That App. All rights reserved.
//

import LBTAComponents
let twitterColor = 	UIColor(r:61,g: 167, b:244)

class UserFooter: DatasourceCell {
    
    let textfooter: UILabel  = {
        
        let label = UILabel()
        label.text = "Show me more"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = twitterColor
        return label
        
    }()
    
    
    override func setupViews() {
        super.setupViews()
        
        let whiteBackgroundView = UIView()
        whiteBackgroundView.backgroundColor = .white
        
        
         addSubview(whiteBackgroundView)
        addSubview(textfooter)
       
        
        whiteBackgroundView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 14, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        textfooter.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 12, bottomConstant: 14, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
}

let textheader: UILabel  = {
    
    let label = UILabel()
    label.text = "WHO TO FOLLOW"
    label.font = UIFont.boldSystemFont(ofSize: 14)
    return label
    
}()


class UserHeader: DatasourceCell {
    override func setupViews() {
        super.setupViews()
        
        backgroundColor = .white
        separatorLineView.isHidden = false
        separatorLineView.backgroundColor = UIColor(r:230, g:230 , b:230 )
        
         addSubview(textheader)
        
        //textheader.fillSuperview()
        
        textheader.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 12, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
    }
}

