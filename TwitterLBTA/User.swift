//
//  User.swift
//  TwitterLBTA
//
//  Created by imran on 6/14/17.
//  Copyright © 2017 Lets Build That App. All rights reserved.
//

import UIKit
import SwiftyJSON

struct User {
    let uname : String
    let username : String
    let biotext : String
    let profileImageUrl: String
    
    init(json: JSON) {
        self.uname = json["name"].stringValue
        self.username = json["username"].stringValue
        self.biotext = json["bio"].stringValue
        self.profileImageUrl = json["profileImageUrl"].stringValue
        
    }
    
}
